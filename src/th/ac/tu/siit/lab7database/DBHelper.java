package th.ac.tu.siit.lab7database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	private static final String DBNAME = "contacts.db";
	private static final int DBVERSION = 1;
	
	public DBHelper(Context ctx) {
		super(ctx, DBNAME, null, DBVERSION);
	}

	//Called when there is no database file in the storage.
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE contacts (" +
				"_id integer primary key autoincrement, " +
				"ct_name text not null, " +
				"ct_phone text not null, " +
				"ct_type integer default 0, " +
				"ct_email text not null);";
		db.execSQL(sql);	
		
		//the primary key of the table has to be "_id"
		//when we want to use ListView to display/manage our data.
	}
	
	
	//Called when the version of the database is increased.
	// Practically, we should use "ALTER TABLE" to avoid losing data.
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql = "DROP TABLE IF EXISTS contacts;";
		db.execSQL(sql);
		this.onCreate(db);
	}

}
